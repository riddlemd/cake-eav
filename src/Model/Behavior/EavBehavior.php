<?php
namespace Riddlemd\Eav\Model\Behavior;

use Cake\ORM\Behavior as BaseBehavior;

use Cake\Event\Event;
use Cake\DataSource\EntityInterface;
use Cake\ORM\Query;
use Cake\Utility\Inflector;
use Cake\ORM\TableRegistry;

class EavBehavior extends BaseBehavior
{
    protected $_defaultConfig = [
        'eavPropertyName' => 'eav_attributes'
    ];

    public function initialize(array $config)
    {
        parent::initialize($config);

        if(!isset($this->_table->EavAttributes))
        {
            $this->_table->hasMany('EavAttributes', [
                'className' => 'Riddlemd/Eav.EavAttributes',
                'foreignKey' => 'fk_id',
                'conditions' => [
                    'fk_resource' => $this->getTable()->getTable()
                ]
            ]);
        }
    }

    public function beforeFind(Event $event, Query $query, \ArrayObject $options, $primary)
    {
        if(!isset($query->getContain()['EavAttributes']))
            $query->contain(['EavAttributes']);
    }

    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        if(!in_array('EavAttributes', $options['associated']))
            $options['associated'][] = 'EavAttributes';

        $schema = $this->_table->getSchema();
        $eavAttributeTable = TableRegistry::get('Riddlemd/Eav.EavAttributes');
        $eavPropertyName = $this->getConfig('eavPropertyName');
        $virtualProperties = $entity->getVirtual();

        $properties = array_filter($entity->visibleProperties(), function($item) use($schema, $virtualProperties) {
            if(!in_array($item, $virtualProperties) && !$schema->getColumn($item) && !$this->_table->hasAssociation(Inflector::camelize($item)))
                return true;
        });

        if(!count($properties))
            return;

        if(!isset($entity->$eavPropertyName) || !is_array($entity->$eavPropertyName))
            $entity->$eavPropertyName = [];

        foreach($properties as $propertyName)
        {
            $eavAttributeExists = false;
            $value = $entity->$propertyName;
            unset($entity->$propertyName);

            foreach($entity->$eavPropertyName as &$eavAttribute)
            {
                if($eavAttribute->name == $propertyName)
                {
                    if($eavAttribute->value !== $value)
                    {
                        $eavAttribute->value = $value;
                        $entity->setDirty($eavPropertyName, true);
                    }
                    return;
                }
            }

            $newAttribute = $eavAttributeTable->newEntity([
                'fk_resource' => $this->getTable()->getTable(),
                'fk_id' => $entity->id,
                'name' => $propertyName,
                'value' => $value
            ]);
            $entity->$eavPropertyName[] = $newAttribute;

            $entity->setDirty($eavPropertyName, true);
        }
    }
}