<?php
namespace Riddlemd\Eav\Model\Table;

use Cake\ORM\Table as BaseTable;

class EavAttributesTable extends BaseTable
{
    public function initialize(array $config)
    {
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        parent::initialize($config);
    }
}
