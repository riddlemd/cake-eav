<?php
namespace Riddlemd\Eav\Model\Entity;

use Cake\I18n\Time;

trait EavAwareEntityTrait
{
    protected function &__eavGet($property)
    {
        $value = &parent::get($property);

        if(!isset($value))
        {
            $eavAttributes = $this->_properties['eav_attributes'] ?? [];
            foreach($eavAttributes as $eavAttribute)
            {
                if($eavAttribute['name'] !== $property) continue;
                
                $value = $eavAttribute->value;

                if(preg_match('/^[\d]{4}-[\d]{2}-[\d]{2}T[\d]{2}:[\d]{2}:[\d]{2}\+[\d]{2}:[\d]{2}$/', $value)) $value = new Time($value);
                if(preg_match('/^[\d]{4}-[\d]{2}-[\d]{2}$/', $value)) $value = new Time($value);

                $this->_properties[$property] = $value;
                break;
            }
        }

        return $value;
    }

    public function &get($property)
    {
        return $this->__eavGet($property);
    }

    public function __eavGetVirtual()
    {
        $vProperties = parent::getVirtual();

        if(!empty($this->_properties['eav_attributes']) && is_array($this->_properties['eav_attributes']))
        {
            foreach($this->_properties['eav_attributes'] as $eavAttribute)
            {
                $vProperties[] = $eavAttribute->name;
            }
        }
        
        return $vProperties;
    }

    public function getVirtual()
    {
        return $this->__eavGetVirtual();
    }

    public function __eavVisibleProperties()
    {
        $properties = parent::visibleProperties();
        $eavAttributesIndex = array_search('eav_attributes', $properties);
        unset($properties[$eavAttributesIndex]);

        foreach($this->_properties['eav_attributes'] as $eavAttribute)
            $properties[] = $eavAttribute->name;
        
        return $properties;
    }

    public function visibleProperties()
    {
        return $this->__eavVisibleProperties();
    }
}